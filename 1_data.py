
# coding: utf-8

# > __CHANGE IF NEED BE__

# In[8]:


data_source = "./../../../git/parlamento/data/"
parliament_count = ["02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13"]


# ________

# __ Dependencies __

# In[9]:


import pandas as pd
import numpy as np
import json
import time


# <br>

# # Biographic Information

# __ Variables __

# In[10]:


col_names_label_bio = ["id", "name", "gender", "birth_date", "birth_year", "profession_law", "legislature", "party", "district"]


# __ Functions __

# In[11]:


def table_bio(source):
    # create DataFrame for parlamentarians
    df_bio = pd.DataFrame(index = col_names_label_bio)
    
    # start index
    row = 0
    
    # parlamentarians list
    ids_not_repeated = []
    
    for count in parliament_count:
        # open file
        data_parlamentarians = json.loads(open(source+"registo-biografico-"+count+".json").read())
        
        for parlamentarian in data_parlamentarians["RegistoBiografico"]["RegistoBiograficoList"]["pt_ar_wsgode_objectos_DadosRegistoBiograficoWeb"]:
            # only one loop per parlamentarian
            if parlamentarian["cadId"] not in ids_not_repeated:
                ids_not_repeated.append(parlamentarian["cadId"])
                
                # get data for each label
                parlamentarian_id = parlamentarian["cadId"]
                parlamentarian_name = parlamentarian["cadNomeCompleto"]
                parlamentarian_gender = parlamentarian["cadSexo"]
                parlamentarian_birth = parlamentarian.get("cadDtNascimento", "missing")
                
                # variable year born
                if parlamentarian_birth[0] == "1":
                    parlamentarian_birthyear = parlamentarian_birth[0:4]
                else:
                    parlamentarian_birthyear = 'missing'
                
                # dummy for law related jobs
                if  parlamentarian.get("cadProfissao", "missing").lower() in ["jurista", "advogado", "advogada"]:
                    parlamentarian_profession_law = 1
                else:
                    parlamentarian_profession_law = 0
                
                legislatures = parlamentarian["cadDeputadoLegis"]["pt_ar_wsgode_objectos_DadosDeputadoLegis"]
                
                # deal with one-time parlamentarians
                if type(legislatures) == dict:
                    legislatures_number = legislatures["legDes"]
                    party = legislatures["gpSigla"]
                    district = legislatures["ceDes"]
                    
                    # add info
                    row += 1
                    df_bio[row] = [parlamentarian_id, parlamentarian_name, parlamentarian_gender,                                    parlamentarian_birth, parlamentarian_birthyear, parlamentarian_profession_law,                                    legislatures_number, party, district]
                    
                else:
                    for i in legislatures:
                        legislatures_number = i["legDes"]
                        party = i["gpSigla"]
                        district = i["ceDes"]
                        
                        # add info
                        row += 1
                        df_bio[row] = [parlamentarian_id, parlamentarian_name, parlamentarian_gender,                                        parlamentarian_birth, parlamentarian_birthyear, parlamentarian_profession_law,                                        legislatures_number, party, district]
                        
                        
    
    return(df_bio.transpose())


# __ Output __

# In[12]:


df1 = table_bio(data_source)


# In[13]:


#df1[df1.id == "163"]


# __Save__

# In[14]:


df1.to_csv("./df1.csv", sep = ",")


# ________

# # Activities of Parlamentarians

# __ Variables __

# In[15]:


col_names_label_act = ["year", "parlamentarian_id", "activity_type", "activity_id"]


# __ Functions __

# In[16]:


def table_act(source):
    # create DataFrame for parlamentarians
    df_act = pd.DataFrame(index = col_names_label_act)
    index = 0
    
    for count in parliament_count:
        # open file
        data_act = json.loads(open(source+"atividade-deputado-"+count+".json").read())
        print("Legis "+str(count)+", act "+str(index)+", "+time.ctime()+".")
        # loop parlamentarians
        for parlamentarian in data_act["ArrayOfAtividadeDeputado"]["AtividadeDeputado"]:
            parlamentarian_id = parlamentarian["deputado"]["depCadId"]
            
            activities = parlamentarian["AtividadeDeputadoList"]["pt_gov_ar_wsar_objectos_ActividadeOut"]
            if activities.get("intev", "missing") != "missing":
                intevs = activities["intev"]["pt_gov_ar_wsar_objectos_IntervencoesOut"]
                if type(intevs) == dict:
                    index += 1
                    inter_id = intevs["intId"]
                    year = intevs['pubDtreu'][0:4]
                    df_act[index] = [year, parlamentarian_id, "intervention", inter_id]
                    
                elif type(intevs) == list:
                    for i in intevs:
                        if i.get("pubDtreu", "missing") != "missing":
                            index += 1
                            inter_id = i["intId"]
                            year = i['pubDtreu'][0:4]
                            df_act[index] = [year, parlamentarian_id, "intervention", inter_id]
                                
    return(df_act.transpose())


# __ Output __

# In[17]:


df2 = table_act(data_source)


# In[18]:


#df2.tail()


# __Save__

# In[19]:


df2.to_csv("./df2.csv", sep = ",")


# ________

# # Initiatives

# __ Variables __

# In[43]:


col_names_label_init = ["init_vote_result", "init_id", "init_year_vote", "count_events", "count_past_votes", "vote_bad", "init_type", "count_men", "count_women", "age_men_mean", "age_women_mean", "age_men_var", "age_women_var", "inter_men_mean", "inter_women_mean", "inter_men_var", "inter_women_var"]


# __ Functions __

# In[44]:


def get_authors_info(authors_listdic, bio, act, init_year_end):    
    
    missing_ages = 0
    if type(authors_listdic) == dict:
        authors_id = authors_listdic["idCadastro"]
        party_men= []
        party_women = []
        
        if bio[bio.id == authors_id].birth_year.values[0] != "missing":    
            if bio[bio.id == authors_id].gender.values[0] == "F":
                count_men = 0
                count_women = 1
                age_men = 0
                age_women = init_year_end - int(bio[bio.id == authors_id].birth_year.values[0])
                inter_men = 0
                inter_women = act[(act['year'].astype(float) < float(init_year_end)) & (act['parlamentarian_id'] == authors_id)].shape[0]
                party_men = 0
                party_women.append(authors_listdic["GP"])
                
            if bio[bio.id == authors_id].gender.values[0] == "M": 
                count_men = 1
                count_women = 0
                age_men = init_year_end - int(bio[bio.id == authors_id].birth_year.values[0])
                age_women = 0
                inter_men = act[(act['year'].astype(float) < float(init_year_end)) & (act['parlamentarian_id'] == authors_id)].shape[0]
                inter_women = 0
                party_men.append(authors_listdic["GP"])

        else:
            missing_ages += 1
            return("missing age")
        
    else:
        count_men = 0
        count_women = 0
        age_men = []
        age_women = []
        inter_men = []
        inter_women = []
        party_men = []
        party_women = []
        
        for i in authors_listdic: 
            if bio[bio.id == i["idCadastro"]].birth_year.values[0] != "missing":
                if bio[bio.id == i["idCadastro"]].gender.values[0] == "F":
                    count_women += 1
                    age_women.append(init_year_end - int(bio[bio.id == i["idCadastro"]].birth_year.values[0]))
                    inter_women.append(act[(act['year'].astype(float) < float(init_year_end)) & (act['parlamentarian_id'] == i["idCadastro"])].shape[0])
                    party_women.append(i["GP"])
                        
                if bio[bio.id == i["idCadastro"]].gender.values[0] == "M":
                    count_men += 1
                    age_men.append(init_year_end - int(bio[bio.id == i["idCadastro"]].birth_year.values[0]))
                    inter_men.append(act[(act['year'].astype(float) < float(init_year_end)) & (act['parlamentarian_id'] == i["idCadastro"])].shape[0])
                    party_men.append(i["GP"])
            else:
                missing_ages += 1
    if count_women == 0 and count_men == 0:
        return("No parlamentarians")
    elif count_women == 0:
        return([count_men, count_women, np.mean(age_men), 0, np.var(age_men), 0, np.mean(inter_men), 0, np.var(inter_men), 0, missing_ages])    
    elif count_men == 0:
        return([count_men, count_women, 0, np.mean(age_women), 0, np.var(age_women), 0, np.mean(inter_women), 0, np.var(inter_women), missing_ages])
    else:
        return([count_men, count_women, np.mean(age_men), np.mean(age_women), np.var(age_men), np.var(age_women), np.mean(inter_men), np.mean(inter_women), np.var(inter_men), np.var(inter_women), missing_ages])


# In[45]:


def get_init_vote(entry):
    
    vote_return = []
    count_past_votes = 0
    count_events = 0
    
    if type(entry) == dict:
        if entry.get("iniEventos", "no_events") != "no_events": 
            for i in entry["iniEventos"]['pt_gov_ar_objectos_iniciativas_EventosOut']:
                count_events += 1
                vote_bad = 0
                
                if type(i) == dict:
                    if i.get("comissao", "no_comissao") != "no_comissao":
                        if type(i["comissao"]["pt_gov_ar_objectos_iniciativas_ComissoesIniOut"]) == dict:
                            vote_result = i["comissao"]["pt_gov_ar_objectos_iniciativas_ComissoesIniOut"]["votacao"]
                            
                            if vote_result == None:
                                vote_bad += 1
                                vote_return.append([0, i["dataFase"], count_events, count_past_votes, vote_bad])
                                count_past_votes += 1
                                
                            else:
                                if vote_result["resultado"] == "Aprovado":
                                    vote_return.append([1, i["dataFase"], count_events, count_past_votes, vote_bad])
                                    count_past_votes += 1
                                    
                                elif vote_result["resultado"] == "Rejeitado":
                                    vote_return.append([0, i["dataFase"], count_events, count_past_votes, vote_bad])
                                    count_past_votes += 1
                                    
                    if i.get("votacao", "no_votacao") != "no_votacao":
                        if type(i["votacao"]["pt_gov_ar_objectos_VotacaoOut"]) == dict:
                            vote_result = i["votacao"]["pt_gov_ar_objectos_VotacaoOut"]
                            
                            if vote_result == None:
                                vote_bad += 1
                                vote_return.append([0, i["dataFase"], count_events, count_past_votes, vote_bad])
                                count_past_votes += 1
                                
                            else:                                    
                                if vote_result["resultado"] == "Aprovado":
                                    vote_return.append([1, i["dataFase"], count_events, count_past_votes, vote_bad])
                                    count_past_votes += 1
                                    
                                elif vote_result["resultado"] == "Rejeitado":
                                    vote_return.append([0, i["dataFase"], count_events, count_past_votes, vote_bad])
                                    count_past_votes += 1
        else:
            return("missing events")
    else:
        return("missing dict")
    
    return(vote_return)


# In[57]:


def table_init(source):
    df_bio = df1#pd.read_csv("./df1.csv", index_col=0)
    df_interventions = df2#pd.read_csv("./df2.csv", index_col=0)
    # create DataFrame for parlamentarians
    df_init = pd.DataFrame(index = col_names_label_init)
    
    # start index
    row = 0
    
    print("Start "+time.ctime()+".")
    
    for count in parliament_count:
        # open file
        data_initiatives = json.loads(open(source+"iniciativas-"+count+".json").read())

        for initiative in data_initiatives["ArrayOfPt_gov_ar_objectos_iniciativas_DetalhePesquisaIniciativasOut"]["pt_gov_ar_objectos_iniciativas_DetalhePesquisaIniciativasOut"]:              
            # get data for each label
            init_id = initiative["iniId"]
            init_type = initiative["iniDescTipo"]
            
            # only add if initiative is finished and has parlamentarians writing it
            try:                                     
                for vote in get_init_vote(initiative):
                    if type(vote) == list:
                        authors = initiative["iniAutorDeputados"]["pt_gov_ar_objectos_iniciativas_AutoresDeputadosOut"]
                        authors_regressors = get_authors_info(authors, df_bio, df_interventions, int(vote[1][0:4]))
                    
                        # add parlamentarian information to table
                        if (authors_regressors not in ["missing age", "No parlamentarians"]) and (authors_regressors[10] == 0):
                            # index
                            row += 1
                            df_init[row] = [vote[0], init_id, vote[1], vote[2], vote[3], vote[4],
                                            init_type, authors_regressors[0], authors_regressors[1], 
                                            authors_regressors[2], authors_regressors[3], authors_regressors[4],
                                            authors_regressors[5], authors_regressors[6], authors_regressors[7],
                                            authors_regressors[8], authors_regressors[9]]
            except KeyError:
                continue
        print("Legis "+str(count)+", ini "+str(row)+", "+time.ctime()+".")
            
    return(df_init.transpose())


# __ Output __

# In[58]:


df3 = table_init(data_source)


# In[59]:


#df3.tail()


# In[18]:


#df3[df3.init_id == "27850"]


# __Save__

# In[20]:


df3.to_csv("./df3.csv", sep = ",")


# <br>

# <br>
